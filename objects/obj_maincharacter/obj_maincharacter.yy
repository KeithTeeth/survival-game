{
    "id": "8a0ef417-1f39-4a7c-aed1-60fe636327c1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_maincharacter",
    "eventList": [
        {
            "id": "43e4f0ee-95b0-496d-9e3d-3f8160735424",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 87,
            "eventtype": 5,
            "m_owner": "8a0ef417-1f39-4a7c-aed1-60fe636327c1"
        },
        {
            "id": "4c676d82-66f2-4c8d-8128-1ca5d23268b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 65,
            "eventtype": 5,
            "m_owner": "8a0ef417-1f39-4a7c-aed1-60fe636327c1"
        },
        {
            "id": "b0d6fc87-6ef3-47d9-8149-50230e8000c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 83,
            "eventtype": 5,
            "m_owner": "8a0ef417-1f39-4a7c-aed1-60fe636327c1"
        },
        {
            "id": "9a520488-4cb3-4dae-9ab9-273865ec2170",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 5,
            "m_owner": "8a0ef417-1f39-4a7c-aed1-60fe636327c1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}