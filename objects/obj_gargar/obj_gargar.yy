{
    "id": "1b01ae20-7f7c-4496-85a7-a2f0655d2991",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gargar",
    "eventList": [
        {
            "id": "f760c743-8ea9-419f-9730-cd7ed852b5e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1b01ae20-7f7c-4496-85a7-a2f0655d2991"
        },
        {
            "id": "e30f60a8-898f-4f4f-9cbf-8e09fe879fec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1b01ae20-7f7c-4496-85a7-a2f0655d2991"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0ca9731a-371c-4594-a618-55e82e8d1f66",
    "visible": true
}