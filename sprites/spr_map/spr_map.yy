{
    "id": "83909f9f-4aba-45fb-abcd-ab84f5d7559a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_map",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1019,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4e51977e-4c77-4d66-9867-227e332b0e92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83909f9f-4aba-45fb-abcd-ab84f5d7559a",
            "compositeImage": {
                "id": "5541716e-7ab0-4fc0-a061-38f3299bd6ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e51977e-4c77-4d66-9867-227e332b0e92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18cfc585-47e7-4978-a472-ad5096f27681",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e51977e-4c77-4d66-9867-227e332b0e92",
                    "LayerId": "e8f94867-c8c3-48fa-857f-f73d9cca8a89"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "e8f94867-c8c3-48fa-857f-f73d9cca8a89",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83909f9f-4aba-45fb-abcd-ab84f5d7559a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}