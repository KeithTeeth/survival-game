{
    "id": "0ca9731a-371c-4594-a618-55e82e8d1f66",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gargar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 122,
    "bbox_left": 6,
    "bbox_right": 124,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b7743ed-6f00-4e03-9eb4-779b49a66156",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ca9731a-371c-4594-a618-55e82e8d1f66",
            "compositeImage": {
                "id": "9b4c35e5-0442-4be0-890e-39e448645fca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b7743ed-6f00-4e03-9eb4-779b49a66156",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cce7e93-b352-4718-9941-fec5a2c0d34b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b7743ed-6f00-4e03-9eb4-779b49a66156",
                    "LayerId": "0ba151ba-c9e3-425c-b78b-549dba61229b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "0ba151ba-c9e3-425c-b78b-549dba61229b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ca9731a-371c-4594-a618-55e82e8d1f66",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}