{
    "id": "c8177fec-0372-461b-9cbe-37e7fe9d932a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_maincharacter",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 48,
    "bbox_right": 79,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2960b9e-1b67-4329-941f-d0e8f3d8381c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8177fec-0372-461b-9cbe-37e7fe9d932a",
            "compositeImage": {
                "id": "e67c2ef9-7743-41cf-b6dc-6ff30ba64578",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2960b9e-1b67-4329-941f-d0e8f3d8381c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8a8328b-6b20-478d-9e3c-c551feb4a7d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2960b9e-1b67-4329-941f-d0e8f3d8381c",
                    "LayerId": "53ce5ee4-b4fb-41c6-9c74-72887641880a"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 128,
    "layers": [
        {
            "id": "53ce5ee4-b4fb-41c6-9c74-72887641880a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c8177fec-0372-461b-9cbe-37e7fe9d932a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}